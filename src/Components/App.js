import React from 'react';
// import { Input } from 'react-toolbox/lib/input';
import ChartComponent from './ChartComponent';
import MaxGenres from './MaxGenres'
import axios from 'axios';
import styled from 'styled-components'

class App extends React.Component {

  constructor(){
    super()
    this.state = {artistName: '', genres: {}}
  }

  addGenre(genre){
    let newObject = this.state.genres
    if (this.state.genres[genre] === undefined) {
      newObject[genre] = 1
      this.setState({genres: newObject})
    } else {
      newObject[genre] += 1
      this.setState({genres: newObject})
    }
  }

  componentDidMount(){
    if (window.location.hash) {
      let token = window.location.hash.split('&')[0].split('=')[1]
      localStorage.setItem('token', token)
      window.location.href = 'http://localhost:3000'
    } else if (localStorage.getItem('token') === null){
      window.location.replace("https://accounts.spotify.com/it/authorize?client_id=fa331740724a48b1b0afd1c3d81e75c8&redirect_uri=http:%2F%2Flocalhost:3000&response_type=token")
    }
  }


//make automcomplete by grabbing source from input and making ajaxquerys

  addArtist(searchTerm){
    var axiosCall = axios.create({
      headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
    });
    axiosCall(`https://api.spotify.com/v1/search?q=${searchTerm}&type=artist`)
    .then((response)=> {
      let genres = response.data.artists.items[0].genres;
      genres.forEach((genre)=>{
        this.addGenre(genre)
      });
    })
    .catch((err)=> console.log(err))
  }

  onChange(event){
    this.setState({artistName: event.target.value})
  }


  render() {

  // this.findMaxGenres()
    return (
      <div>
        <InfoWrapper>
          <Input onChange={this.onChange.bind(this)} type='text' label='Required Field' placeholder={'Please, add one of your favourite artists'} />
          <Button onClick={this.addArtist.bind(this, this.state.artistName)}>Add Artist</Button>
        </InfoWrapper>
        <InfoWrapper>
          <ChartComponent genres={this.state.genres} />
          <MaxGenres  genres={this.state.genres}/>
        </InfoWrapper>
      </div>
    )
  }
}


const Input = styled.input`
  width: 30%;
  line-height: 2em;
  font-size: 1em;
  outline: none;
  text-align: center
`
const Button = styled.button`
  background-color: rgb(43, 206, 70);
  color: white;
  width: 10%;
  line-height: 2em;
  font-size: 1em;
  border: none;
  outline: none;

`

const InfoWrapper = styled.div`
  display: flex;
  margin-top: 2%;
  justify-content: center;
`


export default App;

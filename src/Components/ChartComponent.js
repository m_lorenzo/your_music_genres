import React from 'react'
import Chart from 'chart.js';

var backgroundColor = [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
]

var borderColor = [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
]


class ChartComponent extends React.Component {

  createBackgroundColorsArray(array){

    let newArray = []
    for (var i = 0; i < 10; i++) {
      newArray = newArray.concat(array)
    }
    return newArray
  }

  componentDidMount() {

    let genresArray = Object.keys(this.props.genres)
    var ctx = document.getElementById("myChart");
    this.myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: '# of repeated genre',
                data: [],
                backgroundColor: this.createBackgroundColorsArray(backgroundColor) ,
                borderColor: this.createBackgroundColorsArray(borderColor) ,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    })
  }

  componentDidUpdate(){

    let genresArray = Object.keys(this.props.genres)

    let data = genresArray.map((genre)=>{
      return this.props.genres[genre]
    })

    this.myChart.data.labels = genresArray

    this.myChart.data.datasets.forEach((dataset) => {
        dataset.data = data
    });

    this.myChart.update();
  }

  render() {

    return (
      <div style={{height:'100vh', width: '80%', scrollX: 'auto'}}>
        <canvas id="myChart"></canvas>
      </div>
    )
  }
}

export default ChartComponent;

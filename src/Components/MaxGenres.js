import React, { PropTypes } from 'react'

import styled from 'styled-components'

class MaxGenres extends React.Component {

  findMaxGenres(){
    if (Object.keys(this.props.genres).length !== 0) {
      let genresArray = []
      for(var genre in this.props.genres) {
        genresArray.push([genre, this.props.genres[genre]])
      }
      this.findTops(genresArray)
    }
  }


  findTops(genresArray){
    let topGenresArray = []
    if (genresArray.length > 4) {
      for (var i = 0; i < 5; i++) {
        let topGenre = genresArray.reduce((a,b)=>{
          return a[1] >= b[1] ? a : b
        })
        let topIndex = genresArray.indexOf(topGenre)
        topGenresArray.push(genresArray.splice(topIndex, 1)[0])
      }
    }

    this.topGenres = topGenresArray.map((genre, index)=> {return <Li>{index + 1}) {genre[0]}</Li>})
  }

  render () {
    this.findMaxGenres()
    if (this.topGenres) {
      return (
        <Wrapper>
          <h4>Your top genres</h4>
          <Ul>
            {this.topGenres}
          </Ul>
        </Wrapper>
      )
    } else {
      return null
    }


  }
}



const Ul = styled.ul`
  margin-top: 50px;
  list-style-type: none;
  padding:0;
`

const Li = styled.li`
  margin-top: 10px;
`

const Wrapper = styled.div`
  margin: 20px;
`


export default MaxGenres;
